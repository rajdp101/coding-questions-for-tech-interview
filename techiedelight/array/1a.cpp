#include<bits/stdc++.h>
using namespace std;

int main(){
	int t,n,sum;
	cin>>t;

	while(t--){
		cin>>n>>sum;
		int a[n];
		for(int i=0;i<n;i++)
			cin>>a[i];

		unordered_map<int,int>ump;
		vector<pair<int,int>>p,q;

		for(int i=0;i<n;i++){
			if(ump.find(sum-a[i])!=ump.end()){
				p.push_back(make_pair(sum-a[i],a[i]));
				q.push_back(make_pair(ump[sum-a[i]],i));
			}
			ump[a[i]]=i;
		}
		for(int i=0;i<p.size();i++)
			cout<<p[i].first<<" "<<p[i].second<<endl;
		cout<<endl;
		for(auto i:q)
			cout<<i.first<<" "<<i.second<<endl;
	}
	return 0;
}