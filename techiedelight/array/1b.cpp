//just for finding pair not for finding index

#include<bits/stdc++.h>
using namespace std;

int main(){
	int t,n,i,j,sum;
	cin>>t;

	while(t--){
		cin>>n>>sum;
		int a[n];
		for(int i=0;i<n;i++)
			cin>>a[i];
		sort(a,a+n);
		vector<pair<int,int>>p;
		i=0;j=n-1;
		while(i<j){
			if(a[i]+a[j]<sum)
				i++;
			else if(a[i]+a[j]>sum)
				j--;
			else{
				p.push_back(make_pair(a[i],a[j]));
				i++;
				j--;
			}
		}
		for(int i=0;i<p.size();i++)
			cout<<p[i].first<<" "<<p[i].second<<endl;
		for(auto i:p)
			cout<<i.first<<" "<<i.second<<endl;
	}
	return 0;
}